package ru.eka.seaBattle;

import java.util.ArrayList;

/**
 * Класс для реализации игры "Морской бой"
 *
 * @author Куцкая Э.А., 15ИТ18
 */

public class Game {
    public static void main(String[] args) {
        Ship ship = new Ship();
        int account = 0;
        String result;
        ship.setLocation(placement());
        System.out.println("Начнем игру!");

        do {
            account++;
            result = ship.shots(InputCoordinate.input());
            System.out.println(result);
        } while (result != "Убил!");
        System.out.println("Попыток: " + account);
    }

    /**
     * Метод расположения корабля
     *
     * @return расположение корабля
     */

    public static ArrayList<String> placement() {
        ArrayList<String> arrayList = new ArrayList<>();
        int random = (int) (Math.random() * 8);
        arrayList.add(String.valueOf(random));
        arrayList.add(String.valueOf(random + 1));
        arrayList.add(String.valueOf(random + 2));
        return arrayList;
    }
}

