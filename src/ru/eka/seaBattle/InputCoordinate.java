package ru.eka.seaBattle;

import java.util.Scanner;

/**
 * Класс ввода данных
 *
 * @author Куцкая Э.А., 15ИТ18
 */
public class InputCoordinate {

    static Scanner scan = new Scanner(System.in);

    /**
     * Метод ввода координат выстрела
     *
     * @return координаты выстрела
     */

    public static String input() {
        System.out.println("Введите координату от 0 до 9: ");
        String coordinate = scan.nextLine();
        return coordinate;
    }
}
