package ru.eka.seaBattle;

import java.util.ArrayList;

/**
 * Класс представляющий корабль
 *
 * @author Куцкая Э.А., 15ИТ18
 */

public class Ship {
    private ArrayList<String> location;

    public ArrayList<String> getLocation() {
        return location;
    }

    public void setLocation(ArrayList location) {
        this.location = location;
    }

    /**
     * Метод обработки выстрела игрока
     *
     * @param shot выстрел игрока
     * @return "Мимо" - если игрок не попал, "Ранил" - если игрок попал, "Убил" - если игрок полностью уничтожил корабль
     */

    public String shots(String shot) {
        int index = location.indexOf(shot);
        String result = "Мимо!";
        if (index != -1) {
            location.remove(index);
            result = "Ранил!";
        }
        if (location.isEmpty()) {
            result = "Убил!";
        }
        return result;
    }

    @Override
    public String toString() {
        return "Корабль{" +
                "расположение=" + location +
                '}';
    }
}
